package com.dao;
import com.entity.*;

import java.util.*;
public interface BookDao {
	public void add(Book book);
	public void update(Book book);
	public void delete(int id);
	public List<Book> getBooks(String hql);
	public Book getBook(String hql);
}
