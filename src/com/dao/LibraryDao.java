package com.dao;
import com.entity.*;
public interface LibraryDao {
	public Library getLibrary();
	public void update(Library lib);
}
