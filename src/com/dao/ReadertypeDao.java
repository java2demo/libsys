package com.dao;
import com.entity.*;
import java.util.*;
public interface ReadertypeDao {
	public List<Readertype> getReadertypes(String hql);
	public Readertype getReadertype(String hql);
	public void add(Readertype readertype);
	public void update(Readertype readertype);
	public void delete(int id);
}
