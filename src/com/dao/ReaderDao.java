package com.dao;

import java.util.List;
import com.entity.Reader;

public interface ReaderDao {
	public List<Reader> getReaders(String hql);
	public Reader getReader(String hql);
	public void add(Reader reader);
	public void update(Reader reader);
	public void delete(int id);
}
