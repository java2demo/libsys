package com.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.dao.BookDao;
import com.entity.Book;

@Repository
public class BookDaoImpl extends BaseDaoImpl implements BookDao {

	@Override
	public void add(Book book) {
		this.getSession().save(book);

	}

	@Override
	public void update(Book book) {
		Session session=this.getSession();
		session.merge(book);
		session.flush();
	}

	@Override
	public void delete(int id) {
		Session session=this.getSession();
		session.delete(session.get(Book.class, id));
		session.flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> getBooks(String hql) {
		return this.getSession().createQuery(hql).list();
	}

	@Override
	public Book getBook(String hql) {
		return (Book) getSession().createQuery(hql).uniqueResult();
	}

}
