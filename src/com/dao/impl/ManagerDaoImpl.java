package com.dao.impl;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.dao.*;
import com.entity.Manager;
@Repository
public class ManagerDaoImpl extends BaseDaoImpl implements ManagerDao{
	@Override
	public Manager checkLogin(String username, String password) {
		String hql="from Manager where username=:username and password=:password";
		Session session=this.getSession();
		Query query=session.createQuery(hql);
		query.setString("username", username);
		query.setString("password", password);
		return (Manager) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Manager> getManagers(String hql) {
		return this.getSession().createQuery(hql).list();
	}

	@Override
	public void save(Manager manager) {
		this.getSession().save(manager);
		
	}

	@Override
	public void delete(int id) {
		Session session=this.getSession();
		session.delete(session.get(Manager.class, id));
		session.flush();
	}

	@Override
	public Manager getManager(String hql) {
		return (Manager) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public void update(Manager manager) {
		Session session=this.getSession();
		session.merge(manager);
		session.flush();
	}

}
