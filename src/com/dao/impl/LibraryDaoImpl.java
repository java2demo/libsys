package com.dao.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.dao.LibraryDao;
import com.entity.Library;
@Repository
public class LibraryDaoImpl extends BaseDaoImpl implements LibraryDao {

	@Override
	public Library getLibrary() {
		String hql="from Library";
		return (Library) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public void update(Library lib) {
		Session session=this.getSession();
		session.merge(lib);
		session.flush();
	}

}
