package com.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.dao.BooktypeDao;
import com.entity.Booktype;

@Repository
public class BooktypeDaoImpl extends BaseDaoImpl implements BooktypeDao {

	@Override
	public void add(Booktype booktype) {
		this.getSession().save(booktype);

	}

	@Override
	public void update(Booktype booktype) {
		Session session=this.getSession();
		session.merge(booktype);
		session.flush();
	}

	@Override
	public void delete(int id) {
		Session session=this.getSession();
		session.delete(session.get(Booktype.class, id));
		session.flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Booktype> getBooktypes(String hql) {
		return this.getSession().createQuery(hql).list();
	}

	@Override
	public Booktype getBooktype(String hql) {
		return (Booktype) getSession().createQuery(hql).uniqueResult();
	}

}
