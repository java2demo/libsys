package com.dao.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.dao.ParaDao;
import com.entity.Parameter;
@Repository
public class ParaDaoImpl extends BaseDaoImpl implements ParaDao {

	@Override
	public Parameter getParameter(String hql) {
		return (Parameter) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public void update(Parameter para) {
		Session session=this.getSession();
		session.merge(para);
		session.flush();
	}

}
