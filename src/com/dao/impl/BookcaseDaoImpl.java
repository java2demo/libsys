package com.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.dao.BookcaseDao;
import com.entity.Bookcase;

@Repository
public class BookcaseDaoImpl extends BaseDaoImpl implements BookcaseDao {

	@Override
	public void add(Bookcase bookcase) {
		this.getSession().save(bookcase);

	}

	@Override
	public void update(Bookcase bookcase) {
		Session session=this.getSession();
		session.merge(bookcase);
		session.flush();
	}

	@Override
	public void delete(int id) {
		Session session=this.getSession();
		session.delete(session.get(Bookcase.class, id));
		session.flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Bookcase> getBookcases(String hql) {
		return this.getSession().createQuery(hql).list();
	}

	@Override
	public Bookcase getBookcase(String hql) {
		return (Bookcase) getSession().createQuery(hql).uniqueResult();
	}

}
