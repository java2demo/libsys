package com.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.dao.*;
import com.entity.Reader;
@Repository
public class ReaderDaoImpl extends BaseDaoImpl implements ReaderDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<Reader> getReaders(String hql) {
		return getSession().createQuery(hql).list();
	}

	@Override
	public Reader getReader(String hql) {
		return (Reader) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public void add(Reader reader) {
		getSession().save(reader);
		
	}

	@Override
	public void update(Reader reader) {
		Session session=this.getSession();
		session.merge(reader);
		session.flush();
	}

	@Override
	public void delete(int id) {
		Session session=this.getSession();
		session.delete(session.get(Reader.class, id));
		session.flush();
	}

}
