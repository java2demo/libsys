package com.dao.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.dao.BorrowDao;
import com.entity.Borrow;
@Repository
public class BorrowDaoImpl extends BaseDaoImpl implements BorrowDao {

	@Override
	public void add(Borrow borrow) {
		this.getSession().save(borrow);

	}

	@Override
	public void delete(int id) {
		Session session=this.getSession();
		session.delete(session.get(Borrow.class, id));
		session.flush();
	}

	@Override
	public void update(Borrow borrow) {
		Session session=this.getSession();
		session.merge(borrow);
		session.flush();		
	}

	@Override
	public Borrow getBorrow(String hql) {
		return (Borrow) getSession().createQuery(hql).uniqueResult();
	}

}
