package com.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import com.dao.*;
import com.entity.Readertype;
@Repository
public class ReadertypeDaoImpl extends BaseDaoImpl implements ReadertypeDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<Readertype> getReadertypes(String hql) {
		return getSession().createQuery(hql).list();
	}

	@Override
	public Readertype getReadertype(String hql) {
		return (Readertype) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public void add(Readertype readertype) {
		getSession().save(readertype);
		
	}

	@Override
	public void update(Readertype readertype) {
		Session session=this.getSession();
		session.merge(readertype);
		session.flush();
	}

	@Override
	public void delete(int id) {
		Session session=this.getSession();
		session.delete(session.get(Readertype.class, id));
		session.flush();
	}

}
