package com.dao;
import com.entity.*;
import java.util.*;
public interface BookcaseDao {
	public void add(Bookcase bookcase);
	public void update(Bookcase bookcase);
	public void delete(int id);
	public List<Bookcase> getBookcases(String hql);
	public Bookcase getBookcase(String hql);
}
