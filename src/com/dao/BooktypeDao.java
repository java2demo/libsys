package com.dao;
import com.entity.*;
import java.util.*;
public interface BooktypeDao {
	public void add(Booktype booktype);
	public void update(Booktype booktype);
	public void delete(int id);
	public List<Booktype> getBooktypes(String hql);
	public Booktype getBooktype(String hql);
}
