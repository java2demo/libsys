package com.dao;
import com.entity.*;
public interface ParaDao {
	public Parameter getParameter(String hql);
	public void update(Parameter para);
}
