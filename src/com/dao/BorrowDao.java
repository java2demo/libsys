package com.dao;
import com.entity.*;
public interface BorrowDao {
	public void add(Borrow borrow);
	public void delete(int id);
	public void update(Borrow borrow);
	public Borrow getBorrow(String hql);
}
