package com.dao;
import java.util.List;

import com.entity.*;
public interface ManagerDao {
	public Manager checkLogin(String username,String password);
	public List<Manager> getManagers(String hql);
	public Manager getManager(String hql);
	public void save(Manager manager);
	public void delete(int id);
	public void update(Manager manager);
}
