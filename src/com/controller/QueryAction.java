package com.controller;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.entity.*;
import com.service.*;
import java.util.*;
@Controller
public class QueryAction {
	@Resource
	private BookService bService;
	//进入图书查询的页面
	@RequestMapping("bookInfo")
	public String bookInfo(){
		return "bookInfo";
	}
	
	//查询图书
	@RequestMapping("queryBook")
	public String queryBook(HttpServletRequest req,ModelMap model){
		String sKey=req.getParameter("sKey");
		String sValue=req.getParameter("sValue");
		
		List<Book> books= bService.queryBooks(sKey, sValue);
		model.put("books", books);
		return "bookInfo";
	}
}
