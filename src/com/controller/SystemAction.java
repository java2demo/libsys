package com.controller;

import javax.annotation.Resource;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.entity.*;
import com.service.*;
@Controller
public class SystemAction {
	@Resource
	private LibraryService lService;
	@Resource
	private ManagerService mService;
	@Resource
	private ParaService pService;
	@Resource
	private BookcaseService bcService;
	//转到图书馆信息界面
	@RequestMapping("libinfo")
	public String libinfo(ModelMap model){
		Library lib=lService.getLibrary();
		model.put("lib", lib);
		return "libinfo";
	}
	
	//保存对图书馆信息的设置
	@RequestMapping("updateLib")
	public void updateLib(HttpServletRequest req,HttpServletResponse res)
		throws Exception{		
		Library lib=lService.getLibrary();
		String curator=req.getParameter("curator").trim();
		lib.setCurator(curator);
		
		String tel=req.getParameter("libtel").trim();
		lib.setTel(tel);
		
		String address=req.getParameter("address");
		lib.setAddress(address);
		
		String email=req.getParameter("email").trim();
		lib.setEmail(email);
		
		String url=req.getParameter("url");
		lib.setUrl(url);
		
		String createDate=req.getParameter("createDate");
		lib.setCreateDate(createDate);
		
		String intro=req.getParameter("intro");
		lib.setIntroduce(intro);
		lService.updateLibrary(lib);
		
		res.sendRedirect("libinfo.do");
	}
	
	//转到管理员界面
	@RequestMapping("manager")
	public String manager(ModelMap model){
		List<Manager> managers=mService.getAllManagers();
		model.put("managers", managers);
		return "manager";
	}
	
	//添加管理员
	@RequestMapping("addManager")
	public String addManager(){
		return "addManager";
	}
	
	//保存管理员
	@RequestMapping("saveManager")
	public String saveManager(HttpServletRequest req){
		String[] paras=req.getParameterValues("permission");
		String username=req.getParameter("username");
		String password=req.getParameter("password");
		
		Manager manager=new Manager();
		manager.setUsername(username);
		manager.setPassword(password);
		setManagerParas(manager, paras);

		mService.save(manager);
		return "addManager";
	}
	
	//删除管理员
	@RequestMapping("deleteManager")
	public void deleteManager(HttpServletRequest req,HttpServletResponse res)
		throws Exception{
		String mid=req.getParameter("mid");
		mService.delete(mid);
		res.sendRedirect("manager.do");
	}
	
	//修改管理员权限
	@RequestMapping("setManager")
	public String setManager(HttpServletRequest req,ModelMap model){
		String mid=req.getParameter("mid");
		Manager manager=mService.getManager(mid);
		model.put("tempManager", manager);
		return "setManager";
	}
	
	//保存对管理员的修改
	@RequestMapping("updateManager")
	public void updateManager(HttpServletRequest req,HttpServletResponse res)
		throws Exception{
		String[] paras=req.getParameterValues("permission");
		String mid=req.getParameter("mid");
		
		Manager manager=mService.getManager(mid);
		setManagerParas(manager, paras);
		
		mService.updateManager(manager);
		res.sendRedirect("manager.do");
	}

	//为manager配置参数
	private void setManagerParas(Manager manager,String[] paras){
		System.out.println("setManagerParas:"+paras.length);
		
		List<String> list=new ArrayList<String>();
		for(String para:paras){
			list.add(para);
		}
		manager.setSysset(list.contains("sysset"));
		manager.setBookset(list.contains("bookset"));
		manager.setReaderset(list.contains("readerset"));
		manager.setBorrowback(list.contains("borrowback"));
		manager.setSysquery(list.contains("sysquery"));
	}
	
	//设置参数
	@RequestMapping("setPara")
	public String setPara(ModelMap model){
		Parameter para=pService.getParameter();
		model.put("para", para);
		return "setPara";
	}
	
	//保存参数设置
	@RequestMapping("updatePara")
	public String updatePara(HttpServletRequest req,ModelMap model){
		String cost=req.getParameter("cost");
		String validity=req.getParameter("validity");
		Parameter para=pService.getParameter();
		para.setCost(Integer.parseInt(cost));
		para.setValidity(Integer.parseInt(validity));
		pService.updateParameter(para);
		
		model.put("okInfo", "参数修改成功！");
		model.put("nextUrl", "setPara.do");
		return "ok";
	}
	//设置书架信息
	@RequestMapping("bookcase")
	public String setBookcase(ModelMap model){
		List<Bookcase> bookcases=bcService.getBookcases();
		model.put("bookcases", bookcases);
		return "bookcase";
	}
	
	//添加书架
	@RequestMapping("addBookcase")
	public String addBookcase(){
		return "addBookcase";
	}
	
	//保存书架
	@RequestMapping("saveBookcase")
	public String saveBookcase(HttpServletRequest req,ModelMap model){
		String bookcaseName=req.getParameter("name");
		Bookcase bookcase=new Bookcase();
		bookcase.setBookcaseName(bookcaseName);
		bcService.addBookcase(bookcase);
		
		model.put("okInfo", "添加书架成功！");
		model.put("nextUrl", "bookcase.do");
		return "ok";
	}
	
	//删除书架
	@RequestMapping("deleteBookcase")
	public String deleteBookcase(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		bcService.deleteBookcase(id);
		
		model.put("okInfo", "删除书架成功！");
		model.put("nextUrl", "bookcase.do");
		return "ok";
	}
	
	//修改书架信息
	@RequestMapping("setBookcase")
	public String setBookcase(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		Bookcase tempBookcase=bcService.getBookcase(id);
		model.put("tempBookcase", tempBookcase);
		return "setBookcase";
	}
	
	//保存书架信息的修改
	@RequestMapping("updateBookcase")
	public String updateBookcase(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		String name=req.getParameter("name");
		
		Bookcase temp=bcService.getBookcase(id);
		temp.setBookcaseName(name);
		
		bcService.updateBookcase(temp);
		
		model.put("okInfo", "修改书架成功！");
		model.put("nextUrl", "bookcase.do");
		return "ok";
	}
}