package com.controller;

import javax.annotation.Resource;
import com.entity.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.service.*;
import java.util.*;
@Controller
public class CommonAction {
	@Resource
	ManagerService mService;
	@Resource
	BookService bService;
	//进入登录界面
	@RequestMapping("goLogin")
	public String goLogin(HttpServletRequest req){
//		System.out.println("goLogin:"+req.getCharacterEncoding());
		return "login";
	}
	
	//验证用户名和密码
	@RequestMapping("login")
	public String login(HttpServletRequest req,HttpSession session,ModelMap model){
		String username=req.getParameter("username");
		String password=req.getParameter("password");
		Manager manager=mService.checkLogin(username, password);
		if(manager!=null){
			session.setAttribute("loginManager", manager);
			List<Book> books=bService.getBooks();
			model.put("books", books);
			return "main";
		}
		
		model.put("errorInfo", "用户名或密码错误，请重新输入！");
		model.put("nextUrl", "goLogin.do");
		return "error";
	}
	
	//回到主页，如果未登陆，则转到登陆界面
	@RequestMapping("goIndex")
	public String goIndex(HttpSession session,ModelMap model){
		if(session.getAttribute("loginManager")!=null){
			List<Book> books=bService.getBooks();
			model.put("books", books);
			return "main";
		}
		return "login";
	}
	
	//进入修改密码的页面
	@RequestMapping("changePassword")
	public String changePassword(){
		return "changePassword";
	}
	
	//保存修改的密码
	@RequestMapping("updatePassword")
	public String updatePassword(HttpServletRequest req,HttpSession session,ModelMap model){
		Manager loginManager=(Manager) session.getAttribute("loginManager");
		if(loginManager==null){
			model.put("errorInfo", "请重新登录");
			model.put("nextUrl", "goLogin.do");
			return "error";
		}
		String username=loginManager.getUsername();
		String oldPassword=req.getParameter("oldPassword");
		Manager manager=mService.checkLogin(username, oldPassword);
		if(manager==null){
			model.put("errorInfo", "原密码错误！");
			model.put("nextUrl", "changePassword.do");
			return "error";
		}
		String newPassword=req.getParameter("newPassword");
		manager.setPassword(newPassword);
		mService.updateManager(manager);
		model.put("okInfo", "密码修改成功！");
		model.put("nextUrl", "changePassword.do");
		return "ok";
	}
	
	//退出系统
	@RequestMapping("quit")
	public String quit(HttpSession session){
		if(session.getAttribute("loginManager")!=null){
			session.removeAttribute("loginManager");
		}
		return "login";
	}
}
