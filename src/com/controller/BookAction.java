package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.entity.*;
import com.service.*;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
@Controller
public class BookAction {
	@Resource
	private BooktypeService btService;
	@Resource
	private BookService bService;
	@Resource
	private BookcaseService bcService;
	//显示图书类型
	@RequestMapping("booktype")
	public String booktype(HttpServletRequest req,ModelMap model){
		List<Booktype> booktypes=btService.getBooktypes();
		model.put("booktypes", booktypes);
		return "booktype";		
	}
	
	//进入添加图书类型的页面
	@RequestMapping("addBooktype")
	public String addBooktype(){
		return "addBooktype";
	}
	
	//保存新增的图书
	@RequestMapping("saveBooktype")
	public String saveBooktype(HttpServletRequest req,ModelMap model){
		Booktype booktype=new Booktype();
		booktype.setBooktypeName(req.getParameter("name"));
		booktype.setBorrowDays(Integer.parseInt(req.getParameter("days")));
		
		btService.addBooktype(booktype);
		model.put("okInfo", "添加图书类型成功！");
		model.put("nextUrl", "booktype.do");
		return "ok";
	}
	
	//进入设置图书类型的界面
	@RequestMapping("setBooktype")
	public String setBooktype(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		Booktype booktype=btService.getBooktype(id);
		model.put("booktype", booktype);
		return "setBooktype";
	}
	
	//保存对图书类型的修改
	@RequestMapping("updateBooktype")
	public String updateBooktype(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		Booktype booktype=btService.getBooktype(id);
		booktype.setBooktypeName(req.getParameter("name"));
		booktype.setBorrowDays(Integer.parseInt(req.getParameter("days")));
		
		btService.updateBooktype(booktype);
		model.put("okInfo", "修改图书类型成功！");
		model.put("nextUrl", "booktype.do");
		return "ok";
	}
	
	//删除图书类型
	@RequestMapping("deleteBooktype")
	public String deleteBooktype(HttpServletRequest req,ModelMap model){
		btService.deleteBooktype(req.getParameter("id"));
		
		model.put("okInfo", "删除图书类型成功！");
		model.put("nextUrl", "booktype.do");
		return "ok";
	}
	
	//显示所有的图书
	@RequestMapping("book")
	public String book(ModelMap model){
		List<Book> books=bService.getBooks();
		
		model.put("books", books);
		return "book";
	}
	
	//添加图书信息
	@RequestMapping("addBook")
	public String addBook(ModelMap model){
		List<Booktype> booktypes=btService.getBooktypes();
		model.put("booktypes", booktypes);
		List<Bookcase> bookcases=bcService.getBookcases();
		model.put("bookcases", bookcases);
		return "addBook";
	}
	
	//保存新增图书
	@RequestMapping("saveBook")
	public String saveBook(HttpServletRequest req,ModelMap model){
		Book book=new Book();
		setBookProperties(book,req);
		book.setBorrowTime(0);
		
		bService.addBook(book);
		
		model.put("okInfo", "添加图书信息成功！");
		model.put("nextUrl", "book.do");
		return "ok";
	}

	//进入修改图书信息的页面
	@RequestMapping("setBook")
	public String setBook(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		Book book=bService.getBook(id);
		model.put("book", book);
		List<Booktype> booktypes=btService.getBooktypes();
		model.put("booktypes", booktypes);
		List<Bookcase> bookcases=bcService.getBookcases();
		model.put("bookcases", bookcases);
		return "setBook";
	}
	
	//保存图书修改信息
	@RequestMapping("updateBook")
	public String updateBook(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		Book book=bService.getBook(id);
		setBookProperties(book, req);
		
		bService.updateBook(book);
		model.put("okInfo", "修改图书信息成功！");
		model.put("nextUrl", "book.do");
		return "ok";
	}
	
	//删除图书
	@RequestMapping("deleteBook")
	public String deleteBook(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		bService.deleteBook(id);
		model.put("okInfo", "删除图书信息成功！");
		model.put("nextUrl", "book.do");
		return "ok";
	}

	//设置图书的属性
	private void setBookProperties(Book book, HttpServletRequest req) {
		String barcode=req.getParameter("barcode");
		book.setBarcode(barcode);
		String name=req.getParameter("name");
		book.setBookname(name);
		String author=req.getParameter("author");
		book.setAuthor(author);
		String booktype=req.getParameter("booktype");
		book.setBooktype(btService.getBooktype(booktype));
		String bookcase=req.getParameter("bookcase");
		book.setBookcase(bcService.getBookcase(bookcase));
	}
}
