package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.entity.*;
import com.service.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

@Controller
public class BorrowAction {
	@Resource
	private ReaderService rService;
	@Resource
	private BookService bService;
	@Resource
	private BorrowService brService;
	//进入借书界面
	@RequestMapping("bookborrow")
	public String bookborrow(){
		return "bookborrow";
	}
	
	//通过条形码查询读者
	@RequestMapping("queyrReader")
	public String queyrReader(HttpServletRequest req,ModelMap model){
		String barcode=req.getParameter("barcode");
		Reader borrowReader=rService.queryReader("barcode", barcode);
		if(borrowReader!=null){
			model.put("borrowReader", borrowReader);
			return "bookborrow";
		}
		model.put("errorInfo", "不存在该读者！");
		model.put("nextUrl", "bookborrow.do");
		return "error";
	}
	
	//借书
	@RequestMapping("borrowBook")
	public String borrowBook(HttpServletRequest req,ModelMap model){
		String sKey=req.getParameter("sKey");
		String sValue=req.getParameter("sValue");	
		String readerid=req.getParameter("readerid");
		Reader borrowReader=rService.getReader(readerid);
		Book book=bService.queryBook(sKey, sValue);
		if(book!=null){			
			Borrow borrow=new Borrow();
			borrow.setReader(borrowReader);
			borrow.setBook(book);
			borrow.setBorrowTime("2015-03-20");
			borrow.setBackTime("2015-04-20");
			brService.addBorrow(borrow);
			borrowReader=rService.getReader(readerid);
			model.put("borrowReader", borrowReader);
			return "bookborrow";
		}
		model.put("errorInfo", "不存在该图书！");
		model.put("nextUrl", "queyrReader.do?barcode="+borrowReader.getBarcode());
		return "error";
	}
	
	//还书
	@RequestMapping("returnBook")
	public String returnBook(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");		
		Reader borrowReader=brService.getBorrow(id).getReader();
		brService.deleteBorrow(id);
		model.put("okInfo", "还书成功！");
		model.put("nextUrl", "queyrReader.do?barcode="+borrowReader.getBarcode());
		return "ok";
	}
	
	//续借
	@RequestMapping("reBorrow")
	public String reBorrow(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");		
		Borrow borrow=brService.getBorrow(id);
		Reader borrowReader=borrow.getReader();
		
		borrow.setBackTime("2015-05-20");
		brService.updateBorrow(borrow);
		
		model.put("okInfo", "续借成功！");
		model.put("nextUrl", "queyrReader.do?barcode="+borrowReader.getBarcode());
		return "ok";
	}
}
