package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.entity.*;
import com.service.*;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
@Controller
public class ReaderAction {
	@Resource
	private ReadertypeService rtService;
	@Resource
	private ReaderService rService;
	
	//显示所有读者类型
	@RequestMapping("readertype")
	public String readertype(ModelMap model){
		List<Readertype> readertypes=rtService.getReadertypes();
		model.put("readertypes", readertypes);
		return "readertype";
	}
	
	//转到添加读者类型的界面
	@RequestMapping("addReadertype")
	public String addReadertype(){
		return "addReadertype";
	}
	
	//保存新增的读者类型
	@RequestMapping("saveReadertype")
	public String saveReadertype(HttpServletRequest req,ModelMap model){
		String name=req.getParameter("name");
		String num=req.getParameter("num");
		Readertype rt=new Readertype();
		rt.setReadertypeName(name);
		rt.setReadertypeNumber(Integer.parseInt(num));
		
		rtService.addReadertype(rt);
		model.put("okInfo", "添加读者类型成功！");
		model.put("nextUrl", "readertype.do");
		return "ok";
	}
	
	//进入修改读者类型的页面
	@RequestMapping("setReadertype")
	public String setReadertype(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		Readertype tempReadertype=rtService.getReadertype(id);
		model.put("tempReadertype", tempReadertype);
		return "setReadertype";
	}
	
	//保存读者类型的修改
	@RequestMapping("updateReadertype")
	public String updateReadertype(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		String name=req.getParameter("name");
		String num=req.getParameter("num");
		
		Readertype rt=rtService.getReadertype(id);
		rt.setReadertypeName(name);
		rt.setReadertypeNumber(Integer.parseInt(num));
		
		rtService.updateReadertype(rt);
		model.put("okInfo", "修改读者类型成功！");
		model.put("nextUrl", "readertype.do");
		return "ok";
	}
	
	//删除读者类型
	@RequestMapping("deleteReadertype")
	public String deleteReadertype(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		rtService.deleteReadertype(id);
		model.put("okInfo", "删除读者类型成功！");
		model.put("nextUrl", "readertype.do");
		return "ok";
	}
	
	//显示读者信息
	@RequestMapping("reader")
	public String reader(ModelMap model){
		List<Reader> readers=rService.getReaders();
		model.put("readers", readers);
		return "reader";
	}
	
	//添加读者信息
	@RequestMapping("addReader")
	public String addReader(ModelMap model){
		List<Readertype> readertypes=rtService.getReadertypes();
		model.put("readertypes", readertypes);
		return "addReader";
	}
	
	//保存新增读者
	@RequestMapping("saveReader")
	public String saveReader(HttpServletRequest req,ModelMap model){
		Reader reader=new Reader();
		setReaderProperties(req,reader);
		rService.addReader(reader);
		
		model.put("okInfo", "添加读者信息成功！");
		model.put("nextUrl", "reader.do");
		return "ok";
	}
	
	//设置读者参数
	private void setReaderProperties(HttpServletRequest req,Reader reader){
		reader.setReaderName(req.getParameter("name"));
		reader.setReadertype(rtService.getReadertype(req.getParameter("readertype")));
		reader.setBarcode(req.getParameter("barcode"));
		reader.setIdType(req.getParameter("idType"));
		reader.setIdNumber(req.getParameter("idNum"));
		reader.setTel(req.getParameter("tel"));
		reader.setEmail(req.getParameter("email"));
	}
	
	//转到修改读者的页面
	@RequestMapping("setReader")
	public String setReader(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		Reader tempReader=rService.getReader(id);
		
		List<Readertype> readertypes=rtService.getReadertypes();
		model.put("readertypes", readertypes);
		model.put("tempReader", tempReader);
		return "setReader";
	}
	
	//保存对读者信息的修改
	@RequestMapping("updateReader")
	public String updateReader(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		Reader tempReader=rService.getReader(id);
		
		setReaderProperties(req, tempReader);
		rService.updateReader(tempReader);
		
		model.put("okInfo", "修改读者信息成功！");
		model.put("nextUrl", "reader.do");
		return "ok";
	}
	
	//删除读者
	@RequestMapping("deleteReader")
	public String deleteReader(HttpServletRequest req,ModelMap model){
		String id=req.getParameter("id");
		rService.deleteReader(id);
		model.put("okInfo", "删除读者信息成功！");
		model.put("nextUrl", "reader.do");
		return "ok";
	}
}
