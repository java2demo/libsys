package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Manager entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "manager", catalog = "libsys")
public class Manager implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String username;
	private String password;
	private Boolean sysset;
	private Boolean readerset;
	private Boolean bookset;
	private Boolean borrowback;
	private Boolean sysquery;

	// Constructors

	/** default constructor */
	public Manager() {
	}

	/** full constructor */
	public Manager(String username, String password, Boolean sysset,
			Boolean readerset, Boolean bookset, Boolean borrowback,
			Boolean sysquery) {
		this.username = username;
		this.password = password;
		this.sysset = sysset;
		this.readerset = readerset;
		this.bookset = bookset;
		this.borrowback = borrowback;
		this.sysquery = sysquery;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "username", length = 30)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", length = 30)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "sysset")
	public Boolean getSysset() {
		return this.sysset;
	}

	public void setSysset(Boolean sysset) {
		this.sysset = sysset;
	}

	@Column(name = "readerset")
	public Boolean getReaderset() {
		return this.readerset;
	}

	public void setReaderset(Boolean readerset) {
		this.readerset = readerset;
	}

	@Column(name = "bookset")
	public Boolean getBookset() {
		return this.bookset;
	}

	public void setBookset(Boolean bookset) {
		this.bookset = bookset;
	}

	@Column(name = "borrowback")
	public Boolean getBorrowback() {
		return this.borrowback;
	}

	public void setBorrowback(Boolean borrowback) {
		this.borrowback = borrowback;
	}

	@Column(name = "sysquery")
	public Boolean getSysquery() {
		return this.sysquery;
	}

	public void setSysquery(Boolean sysquery) {
		this.sysquery = sysquery;
	}

}