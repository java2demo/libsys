package com.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Readertype entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "readertype", catalog = "libsys")
public class Readertype implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String readertypeName;
	private Integer readertypeNumber;
	private Set<Reader> readers = new HashSet<Reader>(0);

	// Constructors

	/** default constructor */
	public Readertype() {
	}

	/** full constructor */
	public Readertype(String readertypeName, Integer readertypeNumber,
			Set<Reader> readers) {
		this.readertypeName = readertypeName;
		this.readertypeNumber = readertypeNumber;
		this.readers = readers;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "readertype_name", length = 50)
	public String getReadertypeName() {
		return this.readertypeName;
	}

	public void setReadertypeName(String readertypeName) {
		this.readertypeName = readertypeName;
	}

	@Column(name = "readertype_number")
	public Integer getReadertypeNumber() {
		return this.readertypeNumber;
	}

	public void setReadertypeNumber(Integer readertypeNumber) {
		this.readertypeNumber = readertypeNumber;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "readertype")
	public Set<Reader> getReaders() {
		return this.readers;
	}

	public void setReaders(Set<Reader> readers) {
		this.readers = readers;
	}

}