package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Library entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "library", catalog = "libsys")
public class Library implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String libraryname;
	private String curator;
	private String tel;
	private String address;
	private String email;
	private String url;
	private String createDate;
	private String introduce;

	// Constructors

	/** default constructor */
	public Library() {
	}

	/** full constructor */
	public Library(String libraryname, String curator, String tel,
			String address, String email, String url, String createDate,
			String introduce) {
		this.libraryname = libraryname;
		this.curator = curator;
		this.tel = tel;
		this.address = address;
		this.email = email;
		this.url = url;
		this.createDate = createDate;
		this.introduce = introduce;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "libraryname", length = 50)
	public String getLibraryname() {
		return this.libraryname;
	}

	public void setLibraryname(String libraryname) {
		this.libraryname = libraryname;
	}

	@Column(name = "curator", length = 10)
	public String getCurator() {
		return this.curator;
	}

	public void setCurator(String curator) {
		this.curator = curator;
	}

	@Column(name = "tel", length = 12)
	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	@Column(name = "address", length = 100)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "email", length = 100)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "url", length = 100)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "createDate", length = 10)
	public String getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	@Column(name = "introduce", length = 200)
	public String getIntroduce() {
		return this.introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

}