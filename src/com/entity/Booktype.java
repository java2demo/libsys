package com.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Booktype entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "booktype", catalog = "libsys")
public class Booktype implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String booktypeName;
	private Integer borrowDays;
	private Set<Book> books = new HashSet<Book>(0);

	// Constructors

	/** default constructor */
	public Booktype() {
	}

	/** full constructor */
	public Booktype(String booktypeName, Integer borrowDays, Set<Book> books) {
		this.booktypeName = booktypeName;
		this.borrowDays = borrowDays;
		this.books = books;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "booktype_name", length = 30)
	public String getBooktypeName() {
		return this.booktypeName;
	}

	public void setBooktypeName(String booktypeName) {
		this.booktypeName = booktypeName;
	}

	@Column(name = "borrow_days")
	public Integer getBorrowDays() {
		return this.borrowDays;
	}

	public void setBorrowDays(Integer borrowDays) {
		this.borrowDays = borrowDays;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "booktype")
	public Set<Book> getBooks() {
		return this.books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

}