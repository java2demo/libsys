package com.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Reader entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "reader", catalog = "libsys")
public class Reader implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Readertype readertype;
	private String readerName;
	private String barcode;
	private String idType;
	private String idNumber;
	private String tel;
	private String email;
	private Set<Borrow> borrows = new HashSet<Borrow>(0);

	// Constructors

	/** default constructor */
	public Reader() {
	}

	/** full constructor */
	public Reader(Readertype readertype, String readerName, String barcode,
			String idType, String idNumber, String tel, String email,
			Set<Borrow> borrows) {
		this.readertype = readertype;
		this.readerName = readerName;
		this.barcode = barcode;
		this.idType = idType;
		this.idNumber = idNumber;
		this.tel = tel;
		this.email = email;
		this.borrows = borrows;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "typeid")
	public Readertype getReadertype() {
		return this.readertype;
	}

	public void setReadertype(Readertype readertype) {
		this.readertype = readertype;
	}

	@Column(name = "reader_name", length = 20)
	public String getReaderName() {
		return this.readerName;
	}

	public void setReaderName(String readerName) {
		this.readerName = readerName;
	}

	@Column(name = "barcode", length = 8)
	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	@Column(name = "id_type", length = 10)
	public String getIdType() {
		return this.idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	@Column(name = "id_number", length = 18)
	public String getIdNumber() {
		return this.idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	@Column(name = "tel", length = 11)
	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	@Column(name = "email", length = 100)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "reader")
	public Set<Borrow> getBorrows() {
		return this.borrows;
	}

	public void setBorrows(Set<Borrow> borrows) {
		this.borrows = borrows;
	}

}