package com.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "bookcase", catalog = "libsys")
public class Bookcase implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String bookcaseName;
	private Set<Book> books = new HashSet<Book>(0);

	// Constructors

	/** default constructor */
	public Bookcase() {
	}

	/** full constructor */
	public Bookcase(String bookcaseName, Set<Book> books) {
		this.bookcaseName = bookcaseName;
		this.books = books;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "bookcase_name", length = 30)
	public String getBookcaseName() {
		return this.bookcaseName;
	}

	public void setBookcaseName(String bookcaseName) {
		this.bookcaseName = bookcaseName;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "bookcase")
	public Set<Book> getBooks() {
		return this.books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

}