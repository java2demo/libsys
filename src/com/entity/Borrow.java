package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Borrow entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "borrow", catalog = "libsys")
public class Borrow implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Reader reader;
	private Book book;
	private String borrowTime;
	private String backTime;

	// Constructors

	/** default constructor */
	public Borrow() {
	}

	/** full constructor */
	public Borrow(Reader reader, Book book, String borrowTime, String backTime) {
		this.reader = reader;
		this.book = book;
		this.borrowTime = borrowTime;
		this.backTime = backTime;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "readerid")
	public Reader getReader() {
		return this.reader;
	}

	public void setReader(Reader reader) {
		this.reader = reader;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bookid")
	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@Column(name = "borrow_time", length = 10)
	public String getBorrowTime() {
		return this.borrowTime;
	}

	public void setBorrowTime(String borrowTime) {
		this.borrowTime = borrowTime;
	}

	@Column(name = "back_time", length = 10)
	public String getBackTime() {
		return this.backTime;
	}

	public void setBackTime(String backTime) {
		this.backTime = backTime;
	}

}