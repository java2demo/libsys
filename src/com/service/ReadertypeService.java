package com.service;
import com.dao.*;
import com.entity.*;
import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
@Service
public class ReadertypeService {
	@Resource
	private ReadertypeDao dao;
	
	public List<Readertype> getReadertypes(){
		return dao.getReadertypes("from Readertype");
	}
	
	public Readertype getReadertype(String id){
		String hql="from Readertype where id="+id;
		return dao.getReadertype(hql);
	}
	
	public void addReadertype(Readertype readertype){
		dao.add(readertype);
	}
	
	public void updateReadertype(Readertype readertype){
		dao.update(readertype);
	}
	
	public void deleteReadertype(String id){		
		dao.delete(Integer.parseInt(id));
	}
}
