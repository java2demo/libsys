package com.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.entity.*;
import com.dao.*;
@Service
public class ParaService {
	@Resource
	private ParaDao dao;
	
	public Parameter getParameter(){
		String hql="from Parameter";
		return dao.getParameter(hql);
	}
	
	public void updateParameter(Parameter para){
		dao.update(para);
	}
}
