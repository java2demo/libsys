package com.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.dao.*;
import com.entity.*;
@Service
public class BorrowService {
	@Resource
	private BorrowDao dao;
	
	public void addBorrow(Borrow borrow){
		dao.add(borrow);
	}
	
	public void deleteBorrow(String id){
		dao.delete(Integer.parseInt(id));
	}
	
	public Borrow getBorrow(String id){
		String hql="from Borrow where id="+id;
		return dao.getBorrow(hql);
	}
	
	public void updateBorrow(Borrow borrow){
		dao.update(borrow);
	}
}
