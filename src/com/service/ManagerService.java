package com.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.dao.*;
import com.entity.*;
@Service
public class ManagerService {
	@Resource
	private ManagerDao dao;
	
	public Manager checkLogin(String username,String password){
		return dao.checkLogin(username, password);
	}
	
	public List<Manager> getAllManagers(){
		String hql="From Manager";
		return dao.getManagers(hql);
	}
	
	public void save(Manager manager){
		dao.save(manager);
	}
	
	public void delete(String mid){
		dao.delete(Integer.parseInt(mid));
	}
	
	public Manager getManager(String mid){
		String hql="from Manager where id="+mid;
		return dao.getManager(hql);
	}
	
	public void updateManager(Manager manager){
		dao.update(manager);
	}
}
