package com.service;
import com.dao.*;
import com.entity.*;
import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
@Service
public class ReaderService {
	@Resource
	private ReaderDao dao;
	
	public List<Reader> getReaders(){
		return dao.getReaders("from Reader");
	}
	
	public Reader getReader(String id){
		String hql="from Reader where id="+id;
		return dao.getReader(hql);
	}
	
	public Reader queryReader(String sKey,String sValue){
		String hql="from Reader where "+sKey+"='"+sValue+"'";
		return dao.getReader(hql);
	}
	
	public void addReader(Reader reader){
		dao.add(reader);
	}
	
	public void updateReader(Reader reader){
		dao.update(reader);
	}
	
	public void deleteReader(String id){		
		dao.delete(Integer.parseInt(id));
	}
}
