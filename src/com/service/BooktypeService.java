package com.service;

import org.springframework.stereotype.Service;
import com.dao.*;
import com.entity.*;
import java.util.*;

import javax.annotation.Resource;
@Service
public class BooktypeService {
	@Resource
	private BooktypeDao dao;
	
	public List<Booktype> getBooktypes(){
		String hql="from Booktype";
		return dao.getBooktypes(hql);
	}
	
	public void addBooktype(Booktype booktype){
		dao.add(booktype);
	}
	
	public void updateBooktype(Booktype booktype){
		dao.update(booktype);
	}
	
	public void deleteBooktype(String id){
		dao.delete(Integer.parseInt(id));
	}
	
	public Booktype getBooktype(String id){
		String hql="from Booktype where id="+id;
		return dao.getBooktype(hql);
	}
}
