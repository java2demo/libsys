package com.service;

import org.springframework.stereotype.Service;
import com.dao.*;
import com.entity.*;

import java.util.*;

import javax.annotation.Resource;
@Service
public class BookService {
	@Resource
	private BookDao dao;
	
	public List<Book> getBooks(){
		String hql="from Book order by borrowTime desc";
		return dao.getBooks(hql);
	}
	
	public Book queryBook(String sKey,String sValue){
		String hql="from Book where "+sKey+"='"+sValue+"'";
		
		return dao.getBook(hql);
	}
	
	public void addBook(Book book){
		dao.add(book);
	}
	
	public void updateBook(Book book){
		dao.update(book);
	}
	
	public void deleteBook(String bid){
		dao.delete(Integer.parseInt(bid));
	}
	
	public Book getBook(String id){
		String hql="from Book where id="+id;
		return dao.getBook(hql);
	}

	public List<Book> queryBooks(String sKey, String sValue) {
		String hql="from Book where "+sKey+"='"+sValue+"'";
		return dao.getBooks(hql);
	}
}
