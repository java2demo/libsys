package com.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.dao.*;
import com.entity.Library;
@Service
public class LibraryService {
	@Resource
	private LibraryDao dao;
	
	public Library getLibrary(){
		return dao.getLibrary();
	}
	
	public void updateLibrary(Library lib){
		dao.update(lib);
	}
}
