package com.service;

import org.springframework.stereotype.Service;
import com.dao.*;
import com.entity.*;
import java.util.*;

import javax.annotation.Resource;
@Service
public class BookcaseService {
	@Resource
	private BookcaseDao dao;
	
	public List<Bookcase> getBookcases(){
		String hql="from Bookcase";
		return dao.getBookcases(hql);
	}
	
	public void addBookcase(Bookcase bookcase){
		dao.add(bookcase);
	}
	
	public void updateBookcase(Bookcase bookcase){
		dao.update(bookcase);
	}
	
	public void deleteBookcase(String bid){
		dao.delete(Integer.parseInt(bid));
	}
	
	public Bookcase getBookcase(String id){
		String hql="from Bookcase where id="+id;
		return dao.getBookcase(hql);
	}
}
