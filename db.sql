create database libsys;
show tables;
select * from bookcase;
select * from booktype;
select * from readertype;
select * from reader;
delete from reader where id=4;
select * from borrow;
delete from borrow where id=2 or id=3;
select * from book;
delete from book where id=5;
select * from manager;
update manager set sysset=1 where id=1;
select * from library;
delete from manager where id=9;

select * from parameter;