<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<form action="updateManager.do" method="post">
<input name="mid" value="${tempManager.id }" type="hidden">
<table width="450" align="center" bgcolor="#FFA500">
	<tr><td align="right" width="40%">用户名:</td>
		<td width="60%">${tempManager.username }</td>
	</tr>
	<tr><td align="right">系统设置：</td>
		<td><c:if test="${tempManager.sysset==true }">
			<input type="checkbox" name="permission" checked="checked" value="sysset">
			</c:if>
			<c:if test="${tempManager.sysset==false }" >
			<input type="checkbox" name="permission" value="sysset">
			</c:if>
		</td>
	</tr>
	<tr><td align="right">读者管理：</td>
		<td><c:if test="${tempManager.readerset==true }">
			<input type="checkbox" name="permission" checked="checked" value="readerset">
			</c:if>
			<c:if test="${tempManager.readerset==false }">
			<input type="checkbox" name="permission" value="readerset">
			</c:if>
		</td>
	</tr>
	<tr><td align="right">图书管理：</td>
		<td><c:if test="${tempManager.bookset==true }">
			<input type="checkbox" name="permission" value="bookset" checked="checked">
			</c:if>
			<c:if test="${tempManager.bookset==false }">
			<input type="checkbox" name="permission" value="bookset">
			</c:if>
		</td>
	</tr>
	<tr><td align="right">图书归还：</td>
		<td><c:if test="${tempManager.borrowback==true }">
			<input type="checkbox" name="permission" value="borrowback" checked="checked">
			</c:if>
			<c:if test="${tempManager.borrowback==false }">
			<input type="checkbox" name="permission" value="borrowback">
			</c:if>
		</td>
	</tr>
	<tr><td align="right">系统查询：</td>
		<td><c:if test="${tempManager.sysquery==true }">
			<input type="checkbox" name="permission" value="sysquery" checked="checked">
			</c:if>
			<c:if test="${tempManager.sysquery==false }">
			<input type="checkbox" name="permission" value="sysquery">
			</c:if>
		</td>
	</tr>
	<tr><td>&nbsp;</td>
		<td><input type="submit" value="修改">
			<input type="reset" value="取消"></td>
	</tr>
</table>
</form>
<%@include file="copyright.jsp" %>
</body>
</html>