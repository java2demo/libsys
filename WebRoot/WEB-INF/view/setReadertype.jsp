<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">
	当前位置：读者管理 &gt; 读者类型管理 &gt;&gt;&gt;</td>
</tr>
<tr>
	<td align="center">
	<form action="updateReadertype.do" method="post">
	<input name="id" value="${tempReadertype.id }" type="hidden">
	<table>
	<tr>
		<td>读者类型：</td>
		<td><input name="name" value="${tempReadertype.readertypeName }"></td>
	</tr>
	<tr>
		<td>可借数量：</td>
		<td><input name="num" value="${tempReadertype.readertypeNumber }"></td>
	</tr>
	<tr>
		<td colspan="2">
		<input type="submit" value="修改">
		<input type="reset" value="重置">
		</td>
	</tr>
	</table>
	</form>
	</td>
</tr>
</table>
<%@ include file="copyright.jsp"%>
</body>
</html>