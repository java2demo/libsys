<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="css/menu.css" rel="stylesheet">
	<script type="text/javascript" src="js/menu.js"></script>
</head>
<body>
<div class="total">
<div class="header">
		<div class="blank"></div>
		<div class="out">
			<a href="goIndex.do">首页</a>
		</div>
		
		<c:if test="${sessionScope.loginManager.sysset==true }">
		<div class="out" onmouseout="hide('sysmenu')" onmouseover="show('sysmenu')">
			<a href="#">系统设置</a><br>
			<div id="sysmenu" class="in" >
				<a href="libinfo.do">图书馆信息</a><br>
				<a href="manager.do">管理员设置</a><br>
				<a href="setPara.do">参数设置</a><br>
				<a href="bookcase.do">书架设置</a>
			</div>
		</div>
		</c:if>
		
		<c:if test="${sessionScope.loginManager.readerset==true }">
		<div class="out" onmouseover="show('readermenu')" onmouseout="hide('readermenu')">
			<a href="#">读者管理</a>
			<div id="readermenu" class="in">
				<a href="readertype.do">读者类型管理</a><br>
				<a href="reader.do">读者档案管理</a>
			</div>
		</div>
		</c:if>
		
		<c:if test="${sessionScope.loginManager.bookset==true }">
		<div class="out" onmouseover="show('bookmenu')" onmouseout="hide('bookmenu')">
			<a href="#">图书管理</a>
			<div id="bookmenu" class="in">
				<a href="booktype.do">图书类型设置</a><br>
				<a href="book.do">图书档案管理</a>
			</div>
		</div>
		</c:if>
		
		<c:if test="${sessionScope.loginManager.borrowback==true }">
		<div class="out" onmouseover="show('borrowmenu')" onmouseout="hide('borrowmenu')">
			<a href="bookborrow.do">图书借阅</a>
		</div>
		</c:if>
		
		<c:if test="${sessionScope.loginManager.sysquery==true }">
		<div class="out" onmouseover="show('querymenu')" onmouseout="hide('querymenu')">
			<a href="#">系统查询</a>
			<div id="querymenu" class="in">
				<a href="bookInfo.do">图书档案查询</a><br>
			</div>
		</div>
		</c:if>
		
		<div class="out">
			<a href="changePassword.do">更改口令</a>
		</div>
		<div class="out">
			<a href="quit.do">退出系统</a>
		</div>
	</div>
</div>
</body>
</html>