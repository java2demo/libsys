<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">
	当前位置：图书管理 &gt; 图书档案管理&gt;&gt;&gt;</td>
</tr>
<tr>
	<td align="center">
	<form action="saveBook.do" method="post">
	<table>
	<tr>
		<td>条形码：</td>
		<td><input name="barcode"></td>
	</tr>
	<tr>
		<td>图书名字：</td>
		<td><input name="name"></td>
	</tr>
	<tr>
		<td>图书类型：</td>
		<td align="left"><select name="booktype">
			<c:forEach items="${booktypes }" var="bt">
			<option value="${bt.id }">${bt.booktypeName }</option>
			</c:forEach>
			</select></td>
	</tr>
	<tr>
		<td>作者：</td>
		<td><input name="author"></td>
	</tr>
	<tr>
		<td>书架：</td>
		<td align="left"><select name="bookcase">
			<c:forEach items="${bookcases }" var="bc">
			<option value="${bc.id }">${bc.bookcaseName }</option>
			</c:forEach>
			</select></td>
	</tr>
	<tr>
		<td colspan="2">
		<input type="submit" value="添加">&nbsp;&nbsp;&nbsp;
		<input type="reset" value="重置">
		</td>
	</tr>
	</table>
	</form>
	</td>
</tr>
</table>
<%@ include file="copyright.jsp"%>
</body>
</html>