<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">
	当前位置：系统设置 &gt; 读者档案管理 &gt;&gt;&gt;</td>
</tr>
<tr>
	<td class="word_orange" align="right">
	<a href="addReader.do">
	添加读者类型&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
</tr>
<tr>
	<td align="center">
	<table width="90%" cellpadding="0" cellspacing="0" class="data">
		<tr bgcolor="#F9D16B">
			<td width="13%" >条形码</td>  
	    	<td width="10%" >姓名</td>
	    	<td width="8%" >读者类型</td>
	    	<td width="12%" >证件类型</td>
	    	<td width="20%" >证件号码</td>
	    	<td width="12%" >电话</td>
	    	<td width="15%" >Email</td>
			<td width="10%" >操作</td>
		</tr>	
		<c:forEach items="${readers }" var="reader">
		<tr>
			<td>${reader.barcode }</td>
			<td>${reader.readerName }</td>
			<td>${reader.readertype.readertypeName }</td>
			<td>${reader.idType }</td>
			<td>${reader.idNumber }</td>
			<td>${reader.tel }</td>
			<td>${reader.email }</td>
			<td ><a href="setReader.do?id=${reader.id }">修改</a>
				<a href="deleteReader.do?id=${reader.id }"
					onclick="return confirm('确定要删除该读者信息吗？')">删除</a>
			</td>
		</tr>
		</c:forEach>
	</table>
	</td>
</tr>
</table>
<%@ include file="copyright.jsp"%>
</body>
</html>