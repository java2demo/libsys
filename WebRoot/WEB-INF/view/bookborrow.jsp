<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">
	当前位置：图书借还 &gt; 图书借阅 &gt;&gt;&gt;</td>
</tr>
<tr>
    <td align="center" valign="top" style="padding:5px;">
    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="47" background="image/borrowBackRenew.gif">&nbsp;</td>   	
    </tr>
    <tr>
        <td align="center" height="50"
        	background="image/main_booksort_1.gif" bgcolor="#F8BF73"> 
        <!--查询读者的table开始 -->    
        <form action="queyrReader.do" method="post">
        <table width="90%" border="0" cellpadding="0" cellspacing="0">
        <tr>
           	<td width="24%" >
           		<img src="image/reader_checkbg.jpg">
           	</td>
            <td width="76%">读者条形码：
            	<input name="barcode" size="24">
            	&nbsp;
            	<input type="submit" value="确定">
            </td>
        </tr>
        </table>
        </form>
        <!--查询读者的table结束 -->    
        </td>
    </tr>
    <tr>
    	<td align="center">
    	<!--显示读者信息的table开始 -->  
    	<table width="96%" border="0" cellpadding="0" cellspacing="0">
		<tr>
            <td>姓名：${borrowReader.readerName }</td>                           
            <td>读者类型： ${borrowReader.readertype.readertypeName }</td>                                          
            <td>证件类型：${borrowReader.idType }</td>                		
            <td>证件号码：${borrowReader.idNumber }</td>                     
            <td>可借数量：${borrowReader.readertype.readertypeNumber } &nbsp;册</td>                     	                                              
        </tr>
		</table>
		<!--显示读者信息的table结束 -->  
    	</td>
    </tr>
    <!-- 空一行 -->
    <tr height="20"><td>&nbsp;</td></tr>
    <tr height="32">
    	<td background="image/borrow_if.gif" align="left">
    	<!-- 借书form开始 -->
    	<form action="borrowBook.do" method="post">
    		<input name="readerid" value="${borrowReader.id }" type="hidden">
	    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;添加的依据：
		    <input name="sKey" type="radio" class="noborder" 
		        value="barcode" checked="checked">
	                               图书条形码 &nbsp;&nbsp;
	        <input name="sKey" type="radio" class="noborder" value="bookname">
	                                图书名称&nbsp;&nbsp;
			<input name="sValue" type="text" id="inputkey" size="50">
	        <input type="submit" value="确定">
	    </form>
	    <!-- 借书form结束 -->
    	</td>
    </tr>
    <!-- 空一行 -->
    <tr height="20"><td>&nbsp;</td></tr>
    <tr>
    	<td align="center">
    	<c:if test="${borrowReader.borrows.size()==0 }">
     		暂无借阅信息
     	</c:if>
     	<c:if test="${borrowReader.borrows.size()>0 }">
     	<!-- 显示借阅信息table开始 -->
    	<table class="data" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
		<tr align="center" bgcolor="#F9D16B" height="25">
           <td width="18%" >图书名称</td>
           <td width="13%">借阅时间</td>
           <td width="13%">应还时间</td>
           <td width="13%">作者</td>
           <td width="14%">图书类型</td>
           <td width="14%">书架</td>
           <td>操作</td>
        </tr>
        <c:forEach items="${borrowReader.borrows }" var="br">
        <tr>
        	<td>${br.book.bookname }</td>
        	<td>${br.borrowTime }</td>
        	<td>${br.backTime }</td>
        	<td>${br.book.author }</td>
        	<td>${br.book.booktype.booktypeName }</td>
        	<td>${br.book.bookcase.bookcaseName }</td>
        	<td><a href="returnBook.do?id=${br.id }">还书</a> 
        		<a href="reBorrow.do?id=${br.id }">续借</a></td>
        </tr>
        </c:forEach>
		</table>
		<!-- 显示借阅信息table结束 -->
     	</c:if>   	
    	</td>
    </tr>
    </table>
    </td>
</tr>
</table>
</body>