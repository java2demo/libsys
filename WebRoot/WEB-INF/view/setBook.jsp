<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">
	当前位置：图书管理 &gt; 图书档案管理&gt;&gt;&gt;</td>
</tr>
<tr>
	<td align="center">
	<form action="updateBook.do" method="post">
	<input name="id" value="${book.id }" type="hidden">
	<table>
	<tr>
		<td>条形码：</td>
		<td><input name="barcode" value="${book.barcode }"></td>
	</tr>
	<tr>
		<td>图书名字：</td>
		<td><input name="name" value="${book.bookname }"></td>
	</tr>
	<tr>
		<td>图书类型：</td>
		<td align="left"><select name="booktype">
			<c:forEach items="${booktypes }" var="bt">
			<c:if test="${bt.id==book.booktype.id }">
				<option value="${bt.id }" selected="selected">
				${bt.booktypeName }</option>
			</c:if>
			<c:if test="${bt.id!=book.booktype.id }">
				<option value="${bt.id }">
				${bt.booktypeName }</option>
			</c:if>
			</c:forEach>
			</select></td>
	</tr>
	<tr>
		<td>作者：</td>
		<td><input name="author" value="${book.author }"></td>
	</tr>
	<tr>
		<td>书架：</td>
		<td align="left"><select name="bookcase">
			<c:forEach items="${bookcases }" var="bc">
			<c:if test="${bc.id==book.bookcase.id }">
				<option value="${bc.id }" selected="selected">
					${bc.bookcaseName }</option>
			</c:if>
			<c:if test="${bc.id!=book.bookcase.id }">
				<option value="${bc.id }">${bc.bookcaseName }</option>
			</c:if>
			</c:forEach>
			</select></td>
	</tr>
	<tr>
		<td colspan="2">
		<input type="submit" value="修改">&nbsp;&nbsp;&nbsp;
		<input type="reset" value="重置">
		</td>
	</tr>
	</table>
	</form>
	</td>
</tr>
</table>
<%@ include file="copyright.jsp"%>
</body>
</html>