<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">
	当前位置：读者管理 &gt; 读者档案管理 &gt;&gt;&gt;</td>
</tr>
<tr>
	<td align="center">
	<form action="updateReader.do" method="post">
	<input name="id" value="${tempReader.id }" type="hidden">
	<input name="name" value="${tempReader.readerName }" type="hidden">
	<input name="barcode" value="${tempReader.barcode }" type="hidden">
	<input name="readertype" value="${tempReader.readertype.id }" type="hidden">
	<table cellspacing="0" cellpadding="0" >
	<tr>
		<td>读者姓名：</td>
		<td><input value="${tempReader.readerName }" disabled="disabled"></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
		<td>读者类型：</td>
		<td><input value="${tempReader.readertype.readertypeName }" disabled="disabled" ></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td>&nbsp;&nbsp;条形码：</td>
		<td><input value="${tempReader.barcode }" disabled="disabled"></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td>证件类型：</td>
		<td><select name="idType">
			<option value="身份证">身份证</option>
			<option value="学生证">学生证</option>
			<option value="护照">护照</option>
			</select>
		</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td>证件号码：</td>
		<td><input name="idNum" value="${tempReader.idNumber }"></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td>联系方式：</td>
		<td><input name="tel" value="${tempReader.tel }"></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td>电子邮箱：</td>
		<td><input name="email" value="${tempReader.email }"></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td colspan="2">
		<input type="submit" value="修改">
		<input type="reset" value="重置">
		</td>
	</tr>
	</table>
	</form>
	</td>
</tr>
</table>
<%@ include file="copyright.jsp"%>
</body>
</html>