<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">
	当前位置：图书设置 &gt; 图书档案管理 &gt;&gt;&gt;</td>
</tr>
<tr>
	<td class="word_orange" align="right" width="90%">
	<a href="addBook.do">
	添加图书信息&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
</tr>
<tr>
	<td align="center">
	<table class="data" width="90%" cellpadding="0" cellspacing="0">
	<tr bgcolor="#F9D16b">
		<td width="13%">条形码</td>  
    	<td width="26%">图书名称</td>
    	<td width="15%">图书类型</td>
    	<td width="14%">作者</td>
    	<td width="12%">书架</td>
		<td width="20%" colspan="2">操作</td>
	</tr>
	<c:forEach items="${books }" var="book">
	<tr>
		<td>${book.barcode }</td>
		<td>${book.bookname }</td>
		<td>${book.booktype.booktypeName }</td>
		<td>${book.author }</td>
		<td>${book.bookcase.bookcaseName }</td>
		<td width="10%"><a href="setBook.do?id=${book.id }">修改</a></td>
		<td width="10%"><a href="deleteBook.do?id=${book.id }"
			onclick="return confirm('确定要删除该图书信息吗？')">删除</a></td>
	</tr>
	</c:forEach>
	</table>
	</td>
</tr>
</table>
<%@ include file="copyright.jsp"%>
</body>
</html>