<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" cellspacing="0" 
	bgcolor="#FFFFFF" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">当前位置：系统设置 &gt; 管理员设置 &gt;&gt;&gt;</td>
</tr>
<tr height="22">
	<td align="right"><a href="addManager.do">添加管理员信息&nbsp;&nbsp;</a></td>
</tr>
<tr>
	<td align="center">
	<table class="data" cellpadding="0" cellspacing="0"  >
	<tr align="center" bgcolor="#F9D16B">
 		<td width="26%">管理员名称</td>
	    <td width="12%">系统设置</td>
	    <td width="12%">读者管理</td>
	    <td width="12%">图书管理</td>
	    <td width="11%">图书借还</td>
	    <td width="10%">系统查询</td>
	    <td width="9%">权限设置</td>
	    <td width="8%">删除</td>
	</tr>
	<c:forEach items="${managers }" var="manager">
	<tr>
		<td>${manager.username }</td>
		<td><c:if test="${manager.sysset==true }">
			<input type="checkbox" name="permission" checked="checked">
			</c:if>
			<c:if test="${manager.sysset==false }">
			<input type="checkbox" name="permission">
			</c:if>
		</td>
		<td><c:if test="${manager.readerset==true }">
			<input type="checkbox" name="permission" checked="checked">
			</c:if>
			<c:if test="${manager.readerset==false }">
			<input type="checkbox" name="permission">
			</c:if>
		</td>
		<td><c:if test="${manager.bookset==true }">
			<input type="checkbox" name="permission" checked="checked">
			</c:if>
			<c:if test="${manager.bookset==false }">
			<input type="checkbox" name="permission">
			</c:if>
		</td>
		<td><c:if test="${manager.borrowback==true }">
			<input type="checkbox" name="permission" checked="checked">
			</c:if>
			<c:if test="${manager.borrowback==false }">
			<input type="checkbox" name="permission">
			</c:if>
		</td>
		<td><c:if test="${manager.sysquery==true }">
			<input type="checkbox" name="permission" checked="checked">
			</c:if>
			<c:if test="${manager.sysquery==false }">
			<input type="checkbox" name="permission">
			</c:if>
		</td>
		<td>&nbsp;
			<c:if test="${manager.username!=sessionScope.manager }">
			<a href="setManager.do?mid=${manager.id }">权限设置</a></c:if>
			&nbsp;
		</td>
		<td>&nbsp;
			<c:if test="${manager.username!=sessionScope.loginManager.username }">
			<a href="deleteManager.do?mid=${manager.id }" 
				onclick="return confirm('确定要删除该管理员吗？')">删除</a></c:if>
			&nbsp;
		</td>
	</tr>
	</c:forEach>
	</table>
	</td>
</tr>
</table>	

<%@include file="copyright.jsp" %>
</body>
</html>