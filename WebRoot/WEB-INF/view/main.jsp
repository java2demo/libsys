<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<center>
<table width="778"  border="0" align="center" cellpadding="0"
	 cellspacing="0" bgcolor="#FFFFFF" class="tableBorder_gray">
	<tr height="20">
		<td class="word_orange" align="left">
			当前位置：首页 &gt;&gt;&gt;&nbsp;</td>
	</tr>	 
	<tr height="57">
		<td background="image/main_booksort.gif" width="100%"></td>
	</tr>
	<tr>
		<td align="center">
		<table width="90%" border="1" cellpadding="0" 
				cellspacing="0" bordercolor="#CCCCCC" class="data">
			<tr bgcolor="#F9D16b">
				<td width="10%">排名</td>  
				<td width="11%">条形码</td>  
		    	<td width="24%">图书名称</td>
		    	<td width="13%">图书类型</td>
		    	<td width="14%">作者</td>
		    	<td width="12%">书架</td>
				<td width="16%">借阅次数</td>
			</tr>
			<c:forEach items="${books }" var="book" varStatus="status">
			<tr>
				<td>${status.index+1 }</td>
				<td>${book.barcode }</td>
				<td>${book.bookname }</td>
				<td>${book.booktype.booktypeName }</td>			
				<td>${book.author }</td>
				<td>${book.bookcase.bookcaseName }</td>
				<td>${book.borrowTime }</td>
			</tr>
			</c:forEach>
		</table>
		</td>
	</tr>
	<tr height="20">
		<td background="image/more.GIF">&nbsp;</td>
	</tr>
</table>
</center>
</body>
<%@include file="copyright.jsp" %>
</html>