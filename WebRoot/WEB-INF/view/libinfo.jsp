<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>图书馆管理系统</title>
<link href="css/style.css" rel="stylesheet">
<script language="javascript">
	function checkForm(form){
		for(var i=0;i<form.length;i++){
			if(form.elements[i].value==""){
				alert("请将信息添写完整!");
				form.elements[i].focus();
				return false;
			}
		}
	}
</script>
</head>
<body>
<%@include file="banner.jsp" %>
<%@include file="menu.jsp" %>

<table width="778"  border="0" cellspacing="0" height="510" 
	class="tableBorder_gray" cellpadding="0" align="center" bgcolor="#FFFFFF">
	<tr height="60">
		<td valign="bottom" class="word_orange">
		当前位置：系统设置 &gt; 图书馆信息 &gt;&gt;&gt;</td>
	</tr>
	<tr height="40"><td>&nbsp;</td></tr>
	<tr>
		<td align="center" valign="top">
		<form action="updateLib.do" name="form1" method="post">
		<table>
			<tr>
				<td width="30%" align="right">图书馆名称：</td>
				<td width="70%" align="left">
				<input id="libname" name="libname" value="${lib.libraryname }" disabled="disabled"></td>
			</tr>
			<tr>
				<td align="right">图书馆馆长：</td>
				<td align="left">
				<input id="curator" name="curator" value="${lib.curator }"></td>
			</tr>
			<tr>
				<td align="right">联系电话：</td>
				<td align="left">
				<input id="libtel" name="libtel" value="${lib.tel }"></td>
			</tr>
			<tr>
				<td align="right">图书馆地址：</td>
				<td align="left">
				<input id="address" name="address" value="${lib.address }"></td>
			</tr>
			<tr>
				<td align="right">联系邮箱：</td>
				<td align="left">
				<input id="email" name="email" value="${lib.email }"></td>
			</tr>
			<tr>
				<td align="right">图书馆网站：</td>
				<td align="left">
				<input id="url" name="url" value="${lib.url }"></td>
			</tr>
			<tr>
				<td align="right">建管时间：</td>
				<td align="left">
				<input id="createDate" name="createDate" value="${lib.createDate }">
					(日期格式：2007-11-22)</td>
			</tr>
			<tr>
				<td align="right">图书馆简介：</td>
				<td align="left">
				<textarea  id="intro" name="intro" cols="40" rows="5" 
					style="text-align: left">${lib.introduce }</textarea></td>
			</tr>
			<tr height="65">
				<td>&nbsp;</td>
				<td><input type="submit" class="btn_grey" value="保存" 
					onClick="return checkForm(form1)">&nbsp;      			
       			<input type="reset" class="btn_grey" value="取消"></td>
			</tr>
		</table>
		</form>
		</td>	
	</tr>
</table>
<%@include file="copyright.jsp" %>
</body>
</html>