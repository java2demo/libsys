<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body bgcolor="#FFFFFF">
<%@include file="banner.jsp" %>
<%@include file="menu.jsp" %>
<form action="saveManager.do" method="post">
<table width="450" align="center" bgcolor="#FFFACD">
	<tr><td align="right" width="40%">用户名:</td>
		<td width="60%"><input name="username"></td>
	</tr>
	<tr><td align="right">密码:</td>
		<td><input name="password" type="password"></td>
	</tr>
	<tr><td align="right">系统设置：</td>
		<td><input name="permission" type="checkbox" value="sysset"></td>
	</tr>
	<tr><td align="right">读者管理：</td>
		<td><input name="permission" type="checkbox" value="readerset"></td>
	</tr>
	<tr><td align="right">图书管理：</td>
		<td><input name="permission" type="checkbox" value="bookset"></td>
	</tr>
	<tr><td align="right">图书归还：</td>
		<td><input name="permission" type="checkbox" value="borrowback"></td>
	</tr>
	<tr><td align="right">系统查询：</td>
		<td><input name="permission" type="checkbox" value="sysquery"></td>
	</tr>
	<tr><td>&nbsp;</td>
		<td><input type="submit" value="保存">
			<input type="reset" value="取消"></td>
	</tr>
</table>
</form>
<%@include file="copyright.jsp" %>
</body>
</html>