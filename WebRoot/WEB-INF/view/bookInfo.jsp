<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom" style="text-indent: 15px;">
	当前位置：图书设置 &gt; 图书档案查询 &gt;&gt;&gt;</td>
</tr>
<tr>
	<td align="center">
	<form action="queryBook.do" method="post">
	<table width="95%" height="38"  border="0" cellpadding="0" 
		cellspacing="0" bgcolor="#E3F4F7" class="tableBorder_gray">
  	<tr>
    	<td align="center" bgcolor="#F9D16B">
		&nbsp;<img src="image/search.gif" width="45" height="28"></td>
    	<td bgcolor="#F9D16B">请选择查询依据：
	      	<select name="sKey" class="wenbenkuang">
	        	<option value="barcode">条形码</option>
	       		<option value="booktype.booktypeName">类别</option>
	        	<option value="bookname" selected>书名</option>
	        	<option value="author">作者</option>
	        	<option value="bookcase.bookcaseName">书架</option>
	        </select>
	      	<input name="sValue">
	      	<input type="submit" value="查询">
      	</td>
    </tr>
    </table>
    </form>
	</td>
</tr>
<tr height="2"><td>&nbsp;</td></tr>
<c:if test="${books!=null }">
<tr>
	<td align="center">
	<table class="data" width="90%" cellpadding="0" cellspacing="0">
	<tr bgcolor="#F9D16b">
		<td width="16%">条形码</td>  
    	<td width="30%">图书名称</td>
    	<td width="19%">图书类型</td>
    	<td width="19%">作者</td>
    	<td width="16%">书架</td>
	</tr>
	<c:forEach items="${books }" var="book">
	<tr>
		<td>${book.barcode }</td>
		<td>${book.bookname }</td>
		<td>${book.booktype.booktypeName }</td>
		<td>${book.author }</td>
		<td>${book.bookcase.bookcaseName }</td>
	</tr>
	</c:forEach>
	</table>
	</td>
</tr>
</c:if>
</table>
<%@ include file="copyright.jsp"%>
</body>
</html>