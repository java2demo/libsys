<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">
	当前位置：系统设置 &gt; 读者类型管理 &gt;&gt;&gt;</td>
</tr>
<tr>
	<td class="word_orange" align="right">
	<a href="addReadertype.do">
	添加读者类型&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
</tr>
<tr>
	<td align="center">
	<table class="data" cellpadding="0" cellspacing="0">
	<tr bgcolor="#F9D16B">
		<td width="35">读者类型</td>
		<td width="35">可借数量</td>
		<td width="30%" colspan="2">操作</td>
	</tr>
	<c:forEach items="${readertypes }" var="rt">
	<tr>
		<td>${rt.readertypeName }</td>
		<td>${rt.readertypeNumber }</td>
		<td width="15%"><a href="setReadertype.do?id=${rt.id }">修改</a></td>
		<td width="15%"><a href="deleteReadertype.do?id=${rt.id }"
			onclick="return confirm('确定要删除该读者类型吗？')">删除</a></td>
	</tr>
	</c:forEach>
	</table>
	</td>
</tr>
</table>
<%@ include file="copyright.jsp"%>
</body>
</html>