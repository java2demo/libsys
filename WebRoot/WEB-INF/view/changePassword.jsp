<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>图书管理系统</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<script type="text/javascript">
	function check(form){
		if(form.oldPassword.value==""){
			alert("请输入旧密码！");
			form.oldPassword.focus();
			return false;
		}
		if(form.newPassword.value==""){
			alert("请输入新密码！");
			return false;
		}
		if(form.newPassword2.value==""){
			alert("请再次输入新密码！");
			return false;
		}
		if(form.newPassword.value!=form.newPassword2.value){
			alert("两次输入新密码不相同，请重新输入！");
			return false;
		}
		return true;
	}
</script>
<body>
<%@include file="banner.jsp"%>
<%@include file="menu.jsp"%>
<table width="778"  border="0" bgcolor="#FFFFFF" 
	cellspacing="0" cellpadding="0" align="center">
<tr height="60">
	<td class="word_orange" valign="bottom">
	当前位置：修改口令 &gt;</td>
</tr>
<tr>
	<td align="center">
	<form name="form1" action="updatePassword.do" method="post">
	<table>
	<tr>
		<td>原密码：</td>
		<td><input name="oldPassword" type="password"></td>
	</tr>
	<tr>
		<td>新密码：</td>
		<td><input name="newPassword" type="password"></td>
	</tr>
	<tr>
		<td>确认新密码：</td>
		<td><input name="newPassword2" type="password"></td>
	</tr>
	<tr>
		<td colspan="2">
		<input type="submit" value="修改" onclick="return check(form1)">
		<input type="reset" value="重置">
		</td>
	</tr>
	</table>
	</form>
	</td>
</tr>
</table>
<%@ include file="copyright.jsp"%>
</body>
</html>